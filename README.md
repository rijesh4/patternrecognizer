Neural Networks

In this project, you will train an Android tablet/phone to recognize hand-written lower-case greek characters. Your project will involve three stages:
Configuration stage: During this stage, the user will select the properties of the neural network - this is done only once for the entire neural net.
Training stage: During this stage, the user will select a pattern to train, and repeatedly draw the pattern on the screen until the neural network learns the pattern.
Testing stage: During this stage, the user will enter patterns one at a time, and the neural network will translate them to typed text.
In your app, a "Landing Activity" will provide buttons for the user to select configuration, training or testing.
Configuration stage

During training stage, your app will go through one activity:
Configuration Activity: The user will select values for:
the grid size (7 X 5, 8 X 6, 9 X 7, or 10 X 8) - the larger the pad, the greater the resolution and the better the learning.
the number of patterns and the names of the patterns that the neural network should learn, e.g., if the network is being constructed to learn the English alphabet, the number is 26 and the names are "a" "b", and so on.
learning constant c (a value in the range 0.05 - 1.0) - this affects the number of training cases needed to learn.
squashing parameter lambda (a value in the range 0.055 - 1.0),
convergence criteria - number of training instances n over which values should remain almost the same and the epsilon difference in weights tolerated from one training instance to the next - for the weights to be considered as having converged.
The parameters selected in this activity should be written to a text file. If this activity is visited again, it should read the values from the text file and display them as the selected values. When a neural network is later constructed, it should consult this text file. (In other words, the above parameters should not be hard-coded into your program.) Typically, the user will set parameters using this activity only once for any neural network. The format for the text file should be as follows:
Number of patterns: 26

Patterns:
alpha
beta
gamma
delta
...

Rows: 10
Columns: 8

c: 0.5
lambda: 0.5
epsilon: 0.001
n: 10

Training Stage

During training stage, your app will go through two activities:
Specification Activity: The user will select the pattern for which the neural network will be trained. The user should be able to select one of the 24 lower-case greek characters (shown on the right in the table), or one of two upper-case characters (shown on the left in the table). The upper-case characters will be be specified and trained during project demonstration.
Training Activity: In this activity, the user will be presented with the following user interface widgets:
A discretizing pad, consisting of a two-dimensional rectangular grid of pixels, whose size was specified in Configuration activity. When the user draws a pattern on the pad, it should display the drawn pattern by turning on the pixels traversed by the input pattern.
"Train" button: After each time the user draws the pattern on the discretizing pad, the user clicks on the Train button, so that the neural network can update its weights.
A counter of the number of examples with which the neural network has been trained for the pattern selected in "Specification Activity"
When the neural network has learned to recognize the selected pattern, it will automatically return to the "Landing Activity".
The neural network will "serialize" or save the weights of its neurons in a resource file. It will update this resource file after learning each new pattern. The resource file may be a text file or an xml file, and its format is left to your discretion. If the neural network contains n neurons and the discretizing pad has rc pixels, each neuron has rc + 1 inputs (the extra input being bias input which is always 1). In all, n * (rc + 1) weights must be saved in the resource file.
Testing Stage

During testing stage, your app will provide a Testing Activity, which consists of the following user interface widgets:
A discretizing pad, the same size as provided for training activity
An "Enter" button that the user will click after entering each pattern on the discretizing pad
A TextField in which the neural network will display the characters (or their names) that the user has entered in the discretizing pad so far. If the neural network does not recognize a pattern, it will display a question mark for the entered pattern.
A "Done" button to end testing.
When the testing stage is launched, the weights saved in the resource file will be used to initialize the neural network.
The Software:

Software Organization: Organize your software as follows:
Define a class called Neuron. It must have at least the following methods (determine the appropriate parameters for each method):
train() - method to train the neuron using one training case
hasConverged() - method that lets the client know that all the neuron weights have converged
getWeights() - method to return the weights of the neuron
respond() - method to be called during testing to determine the neuron's response to test input
It must obtain from the text file produced by Configuration activity, all the constants needed for learning - learning constant c, squashing parameter lambda, tolerance epsilon, and n.
Define a class called NeuralNet which composes Neuron objects. The minimum topology for the NeuralNet is one layer:
It will consist of as many neurons as patterns selected in Configuration activity (in our case, 26) that the network must recognize
Each neuron will have r * c + 1 inputs, r being the number of rows and c the number of columns in the discretizing pad, as specified in the Configuration activity. The extra input is for bias input, which is always 1.
Use sigmoidal threshold function (bipolar function is better than unipolar function). You are welcome to use a reference implementation of Neuron and Neural Net by Shuochuan Xu (spring 2013), but you do not have to.
Report:

Record and report in your technical manual, the number of training instances needed to train each pattern - record these numbers from your Training activity when it reports that it has learned each pattern. Finally, do a comparative analysis of two different grid sizes for at least two different values of learning constant - for each case, calculate the average number of training cases needed for the neural network to learn all the patterns.
 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

//import com.example.testdraw.Square;

public class TheGrid {
	
	public Square[] squareList; 
	public int row; 
	public int column; 
	public float gridWidth; 
	public float gridHeight; 
	public float area;
	public float length; 
	
	public TheGrid(int row, int column , float gridWidth, float gridHeight){
		squareList = new Square [row*column];
		this.row = row; 
		this.column = column;
		this.gridHeight = gridHeight; 
		this.gridWidth = gridWidth; 
		calcArea ();
		initSquares(); 
	}
	
	public void calcArea () {
		area = (gridWidth * gridHeight) / (row * column) ;  
		length = (float)Math.sqrt(area); 
	}
	
	public void initSquares() {
		String a = " "; 
		for (int i = 0 ;  i < row ; i ++ ) {
			for (int j = 0 ; j < column ; j++) {
				squareList[j + i *column ] = new Square(i*length , j*length , area); 
				squareList[j + i *column ].xRaw = i; 
				squareList[j + i *column ].yRaw = j;
			}
		}
		
	}
	
	public Square[] retSquares () {
		return squareList; 
	}
	
}


 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class ConfigActivity extends Activity {
	private int size = 35; 
	private boolean isNew = false; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_config);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.config, menu);
		return true;
	}
	
/** 
	 * function Name: selectSize(View view)
	   Purpose: Selects the size of grid from the config activity
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	1) Switch the selected input
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public void selectSize(View view) {
		
		switch(view.getId()) {
		case R.id.b_sevenBy:
			break;
		
		case R.id.b_eightBy:
			size = 48; 
			break;
			
		case R.id.b_nineBy:
			size = 63; 
			break;
		
		case R.id.b_tenBy:
			size = 80; 
			break;
			
		case R.id.b_new: 
			isNew = true; 
			break;
			
		}
		
	}


/** 
	 * function Name:initSave()
	   Purpose: Initialize save to file 
	   Parameters:  
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	
	public void initSave() {
		
		String text = ""; 
		
		text+= size + "\r\n" ; 
		
		
		//numOfPatterns
		EditText et = (EditText)findViewById(R.id.e_numOfPattern); 
		text+= Integer.parseInt(et.getText().toString()) + "\r\n";
		//LearnConstant
		et = (EditText)findViewById(R.id.e_lc);
		text+= Double.parseDouble(et.getText().toString()) + "\r\n";
		//EpsilonVal
		et = (EditText)findViewById(R.id.e_ev);
		text+= Double.parseDouble(et.getText().toString()) + "\r\n";
		//SqParam
		et = (EditText)findViewById(R.id.e_sp);
		text+= Double.parseDouble(et.getText().toString()) + "\r\n";
		//nn
		et = (EditText)findViewById(R.id.e_nn);
		text+= Integer.parseInt(et.getText().toString()) + "\r\n";
		//SetNewVal
		if (isNew) {
			text+= "1"; 
		}
		else {
			text += "0"; 
		}
		
		
		try {
			File logFile = new File(Environment.getExternalStorageDirectory().toString(), "configFile.txt");
			if(!logFile.exists()) {
			     logFile.createNewFile();
			}

			BufferedWriter output = new BufferedWriter(new FileWriter(logFile));
			output.write(text);
			output.close();
			
		} catch (Exception e) {
		        e.printStackTrace();
		}
		
	}
	

	public void initBaseScreen(View view) {
		
		initSave(); 
		Intent intent = new Intent (this, BaseActivity.class) ;
		startActivity(intent); 
		
	}

}

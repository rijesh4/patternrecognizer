 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.os.Environment;

public class NeuralNet {

	//protected Neuron[] neuronList; 
	Map<Integer, Neuron> m_neurons; 
	boolean m_loadedFromFile = false;
	
	protected double learnC; 
	protected double squashP; 
	protected int convergeCount;
	protected double m_epsilonVal;
	protected int numOfNeurons; 
	protected int numNodes; 
	
	protected String path; 
	//public static int count=0; 
	
	public static NeuralNet neuralnet = new NeuralNet(getFileName()); 


/** 
	 * function Name: NeuralNet 
	   Purpose: Constructor
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	
	public NeuralNet (String fileName) { //int numNeurons,double learnC, double squashP, int numNodes
		
		//count++; 
		path = fileName +".xml"; 
		m_neurons = new HashMap<Integer, Neuron>(); 
		//int numNeurons = 10; 
		
		//neuronList = new Neuron[numNeurons];
		readInitialData("configFile.txt");
		
		/*Neuron n1 = new Neuron(); 
		n1.learnC = learnC; 
		n1.squashP = squashP; 
		n1.m_epsilonVal = m_epsilonVal;*/
		
		for (int i = 0 ; i < numOfNeurons ; i++) {	
			m_neurons.put(i, new Neuron());  
		}
	}

/** 
	 * function Name: getFileName()  
	   Purpose: Statically get the grid size from and set the name of the .xml file to store the NeuralNet data. 
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	
	public static String getFileName() {
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,"configFile.txt");
		String line = ""; 
		int tempNumNodes; 
		String fileN = "weightsy0000000";
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine(); 
			tempNumNodes = Integer.parseInt(line);
			
			 
			
			switch(tempNumNodes) {
				case 35:
					break; 
				case 48:
					fileN ="weightsy0000001" ;
					break;
					//return fileN;
				case 63:
					fileN ="weightsy0000002" ;
					break;
					//return fileN;
				case 80:
					fileN ="weightsy0000003" ;
					break;
					//return fileN;
				default:
					fileN ="weightsy0000000" ;
					break;
					//return fileN;
					
					
			}
			br.close(); 
			
		}catch (IOException e){
			
		}
		return fileN;
	}

/** 
	 * function Name: readInitialData
	   Purpose: Populate the data from the file to create the Neural Net
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	
	public void readInitialData(String fileName) {
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,fileName);
		String line = ""; 
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine(); 
			numNodes = Integer.parseInt(line);
			line = br.readLine();
			numOfNeurons = Integer.parseInt(line); 
			//
			line = br.readLine(); 
			learnC = Double.parseDouble(line); 
			//
			line = br.readLine(); 
			m_epsilonVal = Double.parseDouble(line);
			//
			line = br.readLine(); 
			squashP = Double.parseDouble(line);
			//
			//NN <-----------------------------
			
			br.close(); 
			
		}catch (IOException e){
			
		}
		
	}
	

/** 
	 * function Name: setWeightIfXmlExits()
	   Purpose: Set the values of individual neurons by uploading data from the .xml file 
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/		
	public void setWeightIfXmlExits()
	{
		if (m_loadedFromFile == false)
		{
			m_loadedFromFile = true;
			
			//File xmlFile = new File(Neuron.m_filePath, "weights.xml");
			File sdcard = Environment.getExternalStorageDirectory();
			//Get the text file
			File xmlFile = new File(sdcard,path);
			if (xmlFile.exists())
			{
				Map<Integer, double[]> neurons = Neuron.readFromFile(path);
				Iterator it = neurons.entrySet().iterator();
				while (it.hasNext())
				{
					Map.Entry mEntry = (Map.Entry) it.next();
					Integer curKey = (Integer) mEntry.getKey();
					double[] curWeights = (double[]) mEntry.getValue();
					
					m_neurons.get(curKey).setWeight(curWeights);
				}
			}
		}
	}
	
	
/*	public void resetAllNeuronM()
	{
		Iterator it = m_neurons.entrySet().iterator();
		
		while (it.hasNext())
		{
			Map.Entry mEntry = (Map.Entry) it.next();
			Neuron curNeuron = (Neuron) mEntry.getValue();
			curNeuron.resetCurM();
		}
	} */
	
}

 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainTestActivity extends Activity {
	
	private MainTestView customCanvas;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_test);
		
		customCanvas =(MainTestView) findViewById(R.id.single_test_view);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_test, menu);
		return true;
	}
	
	public void initReset (View view){
        //container.getChildAt(0).clearCircle();
		customCanvas.initReset(); 
    }
	
	public void initTest (View view) {
		//customCanvas.setCurVal(); 
		Integer charVal = customCanvas.initTest();
		TextView tv = (TextView)findViewById(R.id.t_showVal);
		String ab = (String)tv.getText()  + charVal + ",";
		tv.setText(ab);
	}
	
	public void goBack(View view) {
		//customCanvas.curNeuron.saveToFile();
		//Intent intent = new Intent (this, BaseActivity.class) ;
		//startActivity(intent);
		this.finish();
	}
	
	

}

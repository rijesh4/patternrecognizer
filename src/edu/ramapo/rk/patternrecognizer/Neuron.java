 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.XMLOutputter;

import android.os.Environment;
import android.widget.Button;

public class Neuron {
	
	protected double learnC; 
	protected double squashP; 
	protected int convergeCount = 0;
	protected double m_epsilonVal; 
	protected int m_nn = 0; 
	protected int temp_nn_count =0; 
	
	protected int numNodes; 
	protected double [] weights; 
	//protected ArrayList<Square> litList; 
	protected int[] litIntList; 
	
	//public static String m_filePath = "/";
	
	
	//private int trainVal; 
	

/** 
	 * function Name: NeuralNet 
	   Purpose: Constructor
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/		
	public Neuron ( ) { // numNodes does not include the base node //double learnC, double squashP, int numNodes, int trainVal
		/*this.learnC = learnC; 
		this.squashP = squashP; 
		convergeCount = 0; 
		this.numNodes = numNodes; 
		//this.litList = litList; 
		this.trainVal = trainVal;*/ 
		
		
		readInitialData("configFile.txt"); 
		initWeights(); 
		
		
		//readFromFile(); 
		
		
		
	}
	
	public void readInitialData(String fileName) {
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File file = new File(sdcard,fileName);
		String line = ""; 
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			//line = br.readLine(); 
			line = br.readLine();
			numNodes = Integer.parseInt(line); 
			//
			line = br.readLine();
			line = br.readLine(); 
			learnC = Double.parseDouble(line); 
			//
			line = br.readLine(); 
			m_epsilonVal = Double.parseDouble(line);
			//
			line = br.readLine(); 
			squashP = Double.parseDouble(line);
			//
			line = br.readLine(); 
			m_nn = Integer.parseInt(line);
			//NN <-----------------------------
			
			br.close(); 
			
		}catch (IOException e){
			
		}
		
	}
	
	
	
	/*public void setLiList(ArrayList litList){
		this.litList = litList; 
	}*/
	
	/*public int getNodePos (Square sq, int rows) {
		
	}*/

/** 
	 * function Name: SetIntLitList
	   Purpose: Sets the liIntList in this from the parameter input
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/		
	public void setIntLitList (int [] aList) {
		litIntList = aList; 
	}
	
/** 
	 * function Name: initWeights
	   Purpose: initial Weight initialization
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	
	public  void initWeights() {
		weights = new double[numNodes+1]; 
		for (int i  = 0 ; i < numNodes ; i++) {
			weights[i] = 0.5; 
		}
		weights[numNodes] = 1; 
	}
	
	/*public void resetCurM()
	{
		convergeCount = 0;
	}*/
	
	
	public void setWeight(double[] a_curWeights)
	{
		weights = a_curWeights;
	}
	
	
	public double[] getWeights()
	{
		return weights;
	}

/** 
	 * function Name: Train
	   Purpose: Train the current Neuron 
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	
	
	
	/*public void trainAll (int dOutput, int[] a_input, ) {
		
	}*/
	
	
	public int train(int dOutput, int[] a_input) {
		
		double summation = 1; // base input ??  
		
		for (int i = 0 ; i < a_input.length ; i++) {
			summation += (weights[a_input[i]] * 1 ); 
		}
		
		
		double output = 1/ (1 + Math.pow(Math.E, -1 * squashP * summation));
		
		//double output = Math.floor(outputX * 100) / 100;
		
		//double number = 651.5176515121351;
		//if (output < 0.99) {
			//output = Math.round(output * 100);
			//output = output/100;
		//}

		
		
		double deltaWeight = 0;
		//double maxDelta = 0; 
		
		for (int i = 0 ; i < a_input.length ; i++) {
			deltaWeight = learnC * (dOutput - output) * output * (1 - output) * weights[a_input[i]]; 
			//deltaWeight = learnC * (1 - output) * output  * weights[litIntList[i]];
			weights[ a_input[i]] = weights[ a_input[i]] + deltaWeight; 
			
			/*if (maxDelta <= deltaWeight) {
				maxDelta = deltaWeight; 
			}*/
			
		} 
		
		/*if (maxDelta < 0.0009) {
			maxDelta *= 10; 
		}
		if (maxDelta < 0.00009) {
			maxDelta *=  100; 
		}
		if (maxDelta < 0.000009) {
			maxDelta *=  1000; 
		}*/
		
		if (dOutput == 1) {
			int test =checkConvergence (deltaWeight); 
			if (test == 0) {
				return 0; 
			}
			else {
				return convergeCount;
			}
			
		}
		else {
			return 0; 
		}
		 
		 
		 	
		
		
	}
	
//Parameters: none
	//Local Variables: builder - an instance of SAXBuilder class
	//                 xmlFile - a new instance of File class, this will be the actual xml file
	//                 doc - instance of Document class
	//                 rootNode - the root element of the doc class
	//                 it - iterator for the m_neurons variable of the static variable neuralnet
	//                 mEntry - an map entry for the current iterator 
	//                 curKey - current key of the map entry
	//                 neurons - a map of each letter to its corresponding weights
	//                 weight - current weight node of the neurons node      
    //This method declare a new instance of SAXBuilder and a new xmlFile with path as m_filePath and the actual 
	//file name as weights.xml. Then it declare a new Document document object form the build method of SAXBuilder 
	//builder that is building the xmlFile. Then the rootNode element is created by getting the doc.getRootElement
	//Then a new map of character to double[] is initialized, and it is called neurons. Then an iterator of the 
	//member variable m_neurons form the static variable neuralnet is created. For each entry inside the iterator
	//it create a variable curKey from the current key of the entry. Then it declares a double array of weights.
	//Then it create a neuron of type Element, by using rootNode.getChild("Neuron" + curKey). The reason was 
	//because I created these elements the same way, so it is logical to retrieve them the same way. Finally
	//it loops through 73 times to bring the weight value of each Neuron inside the xml into the weights double array.
	//While still inside the current iteration, the map pair: curKey, and weights are being added to the neurons map.
	//finally after all the iteration. The map neurons is being returned. If everything fails, it simply return null.	
	static public Map<Integer, double[]> readFromFile(String path)
	{
		SAXBuilder builder = new SAXBuilder();
		//File xmlFile = new File(m_filePath, "weights.xml");
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File xmlFile = new File(sdcard,path);
		
		try {
			Document doc = (Document) builder.build(xmlFile);
			Element rootNode = doc.getRootElement();
			Map<Integer, double[]> neurons = new HashMap<Integer, double[]>();
			
			NeuralNet.neuralnet.m_neurons.get(1);
			Iterator it = NeuralNet.neuralnet.m_neurons.entrySet().iterator();
			while (it.hasNext())
			{
				Map.Entry mEntry = (Map.Entry) it.next();
				Integer curKey = (Integer) mEntry.getKey();
				
				double[] weights = new double[NeuralNet.neuralnet.numNodes + 1];
				Element neuron = rootNode.getChild("Neuron" + curKey);
				
				for (int i = 0; i < NeuralNet.neuralnet.numNodes; i++)
				{
					Element weight = neuron.getChild("Weight" + i);
					weights[i] = Double.parseDouble(weight.getText());
				}
				neurons.put(curKey, weights);//get the xml element and store it
			}
			
			return neurons;
			
		} catch (JDOMException e) {
		} catch (IOException e) {
		}
		return null;
	}
	

//Parameters: none
	//Local Variables: builder - an instance of SAXBuilder class
	//                 xmlFile - a new instance of File class, this will be the actual xml file
	//                 doc - instance of Document class
	//                 rootNode - the root element of the doc class
	//                 it - iterator for the m_neurons variable of the static variable neuralnet
	//                 mEntry - an map entry for the current iterator 
	//                 curKey - current key of the map entry
	//                 curNeuron - current Neuron object of the map entry
	//                 curWeights - current Weights of the curNeuron
	//                 neurons - current child node of the rootNode
	//                 weight - current weight node of the neurons node
	//                 xmlOutput - the out putter for the built XML
    //This method first initialize a new SAXBuilder called builder, then a new File xmlFile with path as
	//m_filePath and the actual name as weights.xml. Then it will check if xmlFile exists. If it does not
	//exit, then a new Document object doc is being made, then a type Element called rootNode is also being
	//made, the actual rootNode name would be "NeuronNetwork". Then a new iterator for the static variable
	//neuralnet's member variable m_neurons is being made. It loops through the whole iterator and for each
	//entry, it gets the current key curKey and current value curValue of the map entry. Then we get the current
	//curNeuron's weights be calling getWeights method. Now we create a new child element called neurons, the actual
	//xml tag would be "Neuron" plus whatever the key is. Then for each neurons element. we are adding 73 weights
	//element and each new weight element has a tag of "Weight" plus whatever the iteration number is. Then for each
	//weight element, the corresponding current weight is also added as the text of the Element. After all that, it
	//combine all the nodes together by the correct child node to its parent. Lastly, we declare an instance of the
	//XMLOutputter and output the file with a FileWriter. Now on the other hand, if the xmlFIle exits, then it uses
	//the predefined SAXBuilder builder to build the XML from xmlFile. Then the rootNode would simply be doc.getRootElement().
	//the iterator would still be the same as above, And for each entry of the iterator,the method of obtaining mEntry, curKey, 
	//curNeuron, curWeights, and neurons would all still be the same. The way weights element built are also the same.	
	
	public void saveToFile()
	{
		SAXBuilder builder = new SAXBuilder();	
		//File xmlFile = new File(m_filePath, "weights.xml");
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		File xmlFile = new File(sdcard,NeuralNet.neuralnet.path);
		
			try {
				if (!xmlFile.exists())
				{
					Document doc = new Document();
					Element rootNode = new Element("NeuronNetwork");
					
					Iterator it = NeuralNet.neuralnet.m_neurons.entrySet().iterator();
					while (it.hasNext())
					{
						Map.Entry mEntry = (Map.Entry) it.next();
						Integer curKey = (Integer) mEntry.getKey();
						Neuron curNeuron = (Neuron) mEntry.getValue();
						
						double[] curWeights = curNeuron.getWeights();
						
						Element neurons = new Element("Neuron" + curKey);
						
						for (int i = 0; i < numNodes; i++)
						{
							Element weight = new Element("Weight" + i);
							weight.setText(Double.toString(curWeights[i]));
							neurons.addContent(weight);
						}
						
						rootNode.addContent(neurons);
					}
					doc.addContent(rootNode);
					
					XMLOutputter xmlOutput = new XMLOutputter();
					xmlOutput.output(doc, new FileWriter(xmlFile.getAbsolutePath()));
				}
				else
				{
					Document doc = (Document) builder.build(xmlFile);					
					Element rootNode = doc.getRootElement();
					
					Iterator it = NeuralNet.neuralnet.m_neurons.entrySet().iterator();
					while (it.hasNext())
					{
						Map.Entry mEntry = (Map.Entry) it.next();
						Integer curKey = (Integer) mEntry.getKey();
						Neuron curNeuron = (Neuron) mEntry.getValue();
						
						double[] curWeights = curNeuron.getWeights();
						
						Element neurons = rootNode.getChild("Neuron" + curKey);
						
						for (int i = 0; i < numNodes; i++)
						{
							Element weight = neurons.getChild("Weight" + i);
							weight.setText(Double.toString(curWeights[i]));
						}
					}
					
					XMLOutputter xmlOutput = new XMLOutputter();
					xmlOutput.output(doc, new FileWriter(xmlFile.getAbsolutePath()));
				}								
				
			} catch (JDOMException e) {
			} catch (IOException e) {
			}
	}
	
	
	
/** 
	 * function Name: checkConvergence
	   Purpose: Checks if the training has converged. 
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/		
	public int checkConvergence (double deltaWeight) {
		if (Double.compare(Math.abs(deltaWeight) , m_epsilonVal) > 0 ){
			convergeCount++;
			temp_nn_count = 0; 
			return 0;
		}
		else {
			temp_nn_count++; 
			if (m_nn <= temp_nn_count) {
				temp_nn_count = 0; 
				
				 
				saveToFile();
				return 1;
				//Button b1 = (Button)findViewById(R.id.b_back);
				/*int duration = 5000;
				Toast toast = Toast.makeText(parent.this.getContext(), " "+ " ", duration);
				toast.show();*/
				//((Activity)this.getContext()).finish();
				//System.exit(1); 
			}
			return 0; 
		}
	}
}

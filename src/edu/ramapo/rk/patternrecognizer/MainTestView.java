 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

public class MainTestView extends View {
	
	private Paint paint = new Paint(); 
	private Path path = new Path(); 
	private TheGrid curGrid; 
	//TheGrid tempGrid; 
	Square[] sqList; 
	ArrayList <Square> litList = new ArrayList(); 
	private static int gridSize = readInitialData();; 
	private static float curGridWidth = 1100; 
	private static float curGridHeight = 1100;
	
	public MainTestView(Context context, AttributeSet attrs) {
		 super(context, attrs);
		 
		 paint.setAntiAlias(true);
		 paint.setStrokeWidth(20f);
		 paint.setColor(Color.BLACK);
		 paint.setStyle(Paint.Style.STROKE);
		 paint.setStrokeJoin(Paint.Join.ROUND);		 
		 
		 /*curNeuron = NeuralNet.neuralnet.m_neurons.get(0); 
		 NeuralNet.neuralnet.setWeightIfXmlExits();*/
	 }
	

/** 
	 * function Name: initTest()
	   Purpose: Starts the testing phase of a neuron by passing the selected input to respond function
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	
	public Integer initTest(){
		
		int a[] = getLitPos (); 
		int newA[] = new int[gridSize+1] ; 
		
		for (int i = 0 ; i < gridSize ; i++) {
			newA[i] = 0;
			for (int j =  0 ; j < a.length ; j++) {
				if (a[j] == i) {
					newA[i] = 1; 
				}
			}
		}
		/*int duration = 5000;
		Toast toast = Toast.makeText(this.getContext(), " "+ respond(newA), duration);
		toast.show();*/
		/*TextView tv = (TextView)findViewById(R.id.t_showVal);
		String ab = (String)tv.getText() + "," + respond(newA);
		tv.setText(ab);*/
		return respond(newA);
		 
	}
	
/** 
	 * function Name: readInitialData() 
	   Purpose: Statically read the size of the grid using the configFile
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	public static int readInitialData() {
		File sdcard = Environment.getExternalStorageDirectory();
		//Get the text file
		String fileName = "configFile.txt"; 
		File file = new File(sdcard,fileName);
		String line = ""; 
		int retSize = 35; 
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			line = br.readLine(); 
			//line = br.readLine();
			retSize	= Integer.parseInt(line); 
			br.close(); 
			//
		}catch (IOException e){
			
		}
		
		return retSize; 
	}
	
	 public void initReset () {  
		 //curGrid = new TheGrid(7, 5, curGrid.gridWidth, curGrid.gridWidth); 
		 switch(gridSize) {
		 case 35:
			 curGrid = new TheGrid(7, 5, curGridWidth, curGridHeight);
			 break;
		 case 48:
			 curGrid = new TheGrid(8, 6, curGridWidth, curGridHeight);
			 break;
		 case 63:
			 curGrid = new TheGrid(9, 7, curGridWidth, curGridHeight);
			 break;
		 case 80:
			 curGrid = new TheGrid(10, 8, curGridWidth, curGridHeight);
			 break;
		 default:
			 curGrid = new TheGrid(7, 5, curGridWidth, curGridHeight);
	 }
		 path.reset(); 
		 litList.clear(); 
		 invalidate(); 
	 }
	 
	 public int[] getLitPos () {
		 
		 int retVal [] = new int [litList.size()]; 
		 for (int i = 0 ; i < litList.size() ; i++) {
			 retVal[i] =  litList.get(i).yRaw * curGrid.row + litList.get(i).xRaw; 
		 }
		 
		 return retVal; 
	 }
	 
	 public void retSetLitVal () {
		 for (int i = 0 ; i < litList.size() ; i++) {
			 litList.get(i).isLitUp = true; 
		 }
		 
	 }
	 
	 public TheGrid getOneInstanceOfGrid( float width , float height) {
		 
		 if (curGrid == null) {
			 
			 switch(gridSize) {
				 case 35:
					 return new TheGrid(7, 5, width, height);
				 case 48:
					 return new TheGrid(8, 6, width, height);
				 case 63:
					 return new TheGrid(9, 7, width, height);
				 case 80:
					 return new TheGrid(10, 8, width, height);
				 default:
					 return new TheGrid(7, 5, width, height);
			 }
		 }
		 else {
			 return curGrid; 
		 }
		  
	 }

/** 
	 * function Name: onDraw()
	   Purpose: Draw the grid, and cells touched as colored 
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	 
	 @Override
	 protected void onDraw(Canvas canvas) {
		 //super.onDraw(canvas);
		 //super.onDraw(canvas); 
		 //testNum += " \r\n"; 
		
		 //curGridWidth = 1100; 
		 //curGridHeight = 1000; 
		 curGrid = getOneInstanceOfGrid( curGridWidth , curGridHeight );  
		 //curGrid = new TheGrid(7,5,getWidth() , getHeight());
		 sqList = curGrid.retSquares();
		 
		 //setCurVal();
		 
		 
		 for (int i = 0 ; i < sqList.length ; i++) {
			 canvas.drawLine(sqList[i].xPos, sqList[i].yPos, sqList[i].xPos + curGrid.length , sqList[i].yPos , paint);
			 canvas.drawLine(sqList[i].xPos, sqList[i].yPos, sqList[i].xPos  , sqList[i].yPos + curGrid.length, paint);
			 canvas.drawLine(sqList[i].xPos, sqList[i].yPos + curGrid.length, sqList[i].xPos + curGrid.length , sqList[i].yPos + curGrid.length, paint);
			 canvas.drawLine(sqList[i].xPos + curGrid.length, sqList[i].yPos, sqList[i].xPos + curGrid.length , sqList[i].yPos + curGrid.length, paint);
		 }
		 
		 canvas.drawPath(path, paint); 
		 
		 //Rect rt = new Rect(0,0,(int)curGrid.length, (int)curGrid.length); 
		 for (int i = 0 ; i < litList.size() ; i++) {
			 //if (sqList[i].isLitUp) {
				 //rt = new Rect((int)litList.get(i).xPos,(int)litList.get(i).yPos,(int)curGrid.length, (int)curGrid.length);
				 paint.setColor(Color.BLUE);
				 paint.setStyle(Style.FILL);
				 paint.setAlpha(50);
				 canvas.drawRect((int)litList.get(i).xPos, (int)litList.get(i).yPos, (int)litList.get(i).xPos + (int)curGrid.length, (int)litList.get(i).yPos + (int)curGrid.length, paint); 
			// }
			 
		 }
		 
		 paint.setColor(Color.BLACK);
		 paint.setStyle(Paint.Style.STROKE);
		 paint.setStrokeJoin(Paint.Join.ROUND);
		 
	 } 
	 
	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
		 float eventX = event.getX();
		 float eventY = event.getY();
		 
		// testNum += " " + event.getX() + "  " + event.getY() + " \r\n" ;  
		 
		 switch (event.getAction()) {
	         case MotionEvent.ACTION_DOWN:
	             // Set a new starting point
	             path.moveTo(eventX, eventY);
	             
	             
	             
	             return true;
	         case MotionEvent.ACTION_MOVE:
	             // Connect the points
	             path.lineTo(eventX, eventY);
	             
	             motionSamples(event); 
	             
	             break;
	         default:
	             return false;
		     }
		     // Makes our view repaint and call onDraw
		 	//tempVal = true; 
		     invalidate();
		     return true;

	 }
	 
	 void motionSamples(MotionEvent ev) {
			
			
		     final int historySize = ev.getHistorySize();
		     final int pointerCount = ev.getPointerCount();
		     for (int h = 0; h < historySize; h++) {
		         //System.out.printf("At time %d:", ev.getHistoricalEventTime(h));
		         for (int p = 0; p < pointerCount; p++) {
		             
		        	 float len = curGrid.length; 
		        	 for (int k = 0 ; k < sqList.length ; k++) {
		        		 /*if (sqList[k].isLitUp) {
		        			 continue; 
		        		 }*/
		        		 if (ev.getHistoricalX(p, h) < sqList[k].xPos + len  && ev.getHistoricalX(p, h) > sqList[k].xPos  ) {
		        			 if (ev.getHistoricalY(p, h) < sqList[k].yPos + len  &&  ev.getHistoricalY(p, h) > sqList[k].yPos ) {
		        				 //sqList[k].isLitUp = true; 
		        				 if (!litList.contains(sqList[k])) {
		        					 litList.add(sqList[k]); 
		        				 }
		        				 
		        			 }
		        		 }
		        	 }
		        	 
		        	 //a += "Move Pos :  " + ev.getPointerId(p) + " " +  ev.getHistoricalX(p, h) + " " + ev.getHistoricalY(p, h) + " \r\n"; 
		         }
		     }
		     
		 }
	 

/** 
	 * function Name: respond
	   Purpose: Respond to the user input in Test Phase to compute the neuron implied
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	  public Integer respond(int[] a_inputs)
		{
			Integer outputCharacter = -1;
			double difference = -10000; //default
			double lowCount = 0;
			
			Map<Integer, double[]> neurons = Neuron.readFromFile(NeuralNet.neuralnet.path);		
			Iterator it = neurons.entrySet().iterator();
			while (it.hasNext())
			{
				Map.Entry mEntry = (Map.Entry) it.next();
				Integer curKey = (Integer) mEntry.getKey();
				double[] curWeights = (double[]) mEntry.getValue();
				
				double net = 1;
				
				for (int i = 0; i < gridSize; i++)
				{
					net = net + a_inputs[i] * curWeights[i];
				}
				
				double lambda = NeuralNet.neuralnet.squashP;
				
				double output = 1/ (1 + Math.pow(Math.E, -1 * lambda * net));
				
				double tmp =  output;
				/*if (tmp < 0.9)
				{
					lowCount++;
				}*/
				if (tmp > difference)
				{
					difference = tmp;
					outputCharacter = curKey;
				}
			}
			//if (lowCount == NeuralNet.neuralnet.numOfNeurons)
			if (difference < 0.9)
			{
				return -1;
			}
			return outputCharacter;
		}

}

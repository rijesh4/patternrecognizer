 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
//import edu.ramapo.rk.patternrecognizer.R;

//import com.example.testdraw.Square;
//import com.example.testdraw.TheGrid;

public class MainDrawingView extends View {
	
	private Paint paint = new Paint(); 
	private Path path = new Path(); 
	private TheGrid curGrid; 
	//TheGrid tempGrid; 
	Square[] sqList; 
	ArrayList <Square> litList = new ArrayList(); 
	private static int gridSize = readInitialData();; 
	private static float curGridWidth = 1100; 
	private static float curGridHeight = 1100;
	private int curVal = 0; 

	 
	
	 public static int readInitialData() {
			File sdcard = Environment.getExternalStorageDirectory();
			//Get the text file
			String fileName = "configFile.txt"; 
			File file = new File(sdcard,fileName);
			String line = ""; 
			int retSize = 35; 
			try {
				BufferedReader br = new BufferedReader(new FileReader(file));
				line = br.readLine(); 
				//line = br.readLine();
				retSize	= Integer.parseInt(line); 
				br.close(); 
				//
			}catch (IOException e){
				
			}
			
			return retSize; 
		}
	
	Neuron curNeuron = new Neuron(); 
	
	
	//boolean tempVal = false ; 
	/*String testNum = " "; 
	String a = " "; */ 
	
	 public MainDrawingView(Context context, AttributeSet attrs) {
		 super(context, attrs);
		 
		 paint.setAntiAlias(true);
		 paint.setStrokeWidth(20f);
		 paint.setColor(Color.BLACK);
		 paint.setStyle(Paint.Style.STROKE);
		 paint.setStrokeJoin(Paint.Join.ROUND);		 
		 
		 curNeuron = NeuralNet.neuralnet.m_neurons.get(0); 
		 NeuralNet.neuralnet.setWeightIfXmlExits();
	 }
	 
	 public int initTrain(int curVal) {
		 
		// setCurVal ();
		 this.curVal = curVal; 
		 
		 Neuron tempNeuron = new Neuron(); 
		 
		 Map<Integer, Neuron> neuronsN = NeuralNet.neuralnet.m_neurons;  
			Iterator it = neuronsN.entrySet().iterator();
			while (it.hasNext())
			{
				Map.Entry mEntry = (Map.Entry) it.next();
				Integer curKey = (Integer) mEntry.getKey();
				//double[] curWeights = (double[]) mEntry.getValue();
				
				tempNeuron = (Neuron) mEntry.getValue();
				
				if (curKey != curVal) {
					tempNeuron.setIntLitList(getLitPos());
					tempNeuron.train(0, getLitPos());
				}
				//m_neurons.get(curKey).setWeight(curWeights);
			}
		 
		 curNeuron = NeuralNet.neuralnet.m_neurons.get(curVal);
		 curNeuron.setIntLitList(getLitPos());
		 
		 return curNeuron.train(1 , getLitPos()); 
		 
	 }
	 
	 
/** 
	 * function Name: initReset
	   Purpose: Reset the grid to enable new input
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	 
	 public void initReset () {  
		 //curGrid = new TheGrid(7, 5, curGrid.gridWidth, curGrid.gridWidth); 
		 switch(gridSize) {
		 case 35:
			 curGrid = new TheGrid(7, 5, curGridWidth, curGridHeight);
			 break;
		 case 48:
			 curGrid = new TheGrid(8, 6, curGridWidth, curGridHeight);
			 break;
		 case 63:
			 curGrid = new TheGrid(9, 7, curGridWidth, curGridHeight);
			 break;
		 case 80:
			 curGrid = new TheGrid(10, 8, curGridWidth, curGridHeight);
			 break;
		 default:
			 curGrid = new TheGrid(7, 5, curGridWidth, curGridHeight);
	 }
		 path.reset(); 
		 litList.clear(); 
		 invalidate(); 
	 }

/** 
	 * function Name: getLitPos
	   Purpose: return the list of cells that has been lit 
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/	 
	 public int[] getLitPos () {
		 
		 int retVal [] = new int [litList.size()]; 
		 for (int i = 0 ; i < litList.size() ; i++) {
			 retVal[i] =  litList.get(i).yRaw * curGrid.row + litList.get(i).xRaw; 
		 }
		 
		 return retVal; 
	 }
	
/** 
	 * function Name: retSetLitVal
	   Purpose: get Lit pos based on isLit boolean val. Unused
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/ 
	 public void retSetLitVal () {
		 for (int i = 0 ; i < litList.size() ; i++) {
			 litList.get(i).isLitUp = true; 
		 }
		 
	 }
	 
/** 
	 * function Name: getOneInstanceOfGrid
	   Purpose: Makes sure singularity of the Grid
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	 public TheGrid getOneInstanceOfGrid( float width , float height) {
		 
		 if (curGrid == null) {
			 
			 switch(gridSize) {
				 case 35:
					 return new TheGrid(7, 5, width, height);
				 case 48:
					 return new TheGrid(8, 6, width, height);
				 case 63:
					 return new TheGrid(9, 7, width, height);
				 case 80:
					 return new TheGrid(10, 8, width, height);
				 default:
					 return new TheGrid(7, 5, width, height);
			 }
		 }
		 else {
			 return curGrid; 
		 }
		  
	 }
	
 /** 
	 * function Name: onDraw
	   Purpose: Draws the Grid of specified size, and colors the lit cells
	   Parameters: 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm: 
	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	 @Override
	 protected void onDraw(Canvas canvas) {
		 //super.onDraw(canvas);
		 //super.onDraw(canvas); 
		 //testNum += " \r\n"; 
		
		 //curGridWidth = 1100; 
		 //curGridHeight = 1000; 
		 curGrid = getOneInstanceOfGrid( curGridWidth , curGridHeight );  
		 //curGrid = new TheGrid(7,5,getWidth() , getHeight());
		 sqList = curGrid.retSquares();
		 
		 //setCurVal();
		 
		 
		 for (int i = 0 ; i < sqList.length ; i++) {
			 canvas.drawLine(sqList[i].xPos, sqList[i].yPos, sqList[i].xPos + curGrid.length , sqList[i].yPos , paint);
			 canvas.drawLine(sqList[i].xPos, sqList[i].yPos, sqList[i].xPos  , sqList[i].yPos + curGrid.length, paint);
			 canvas.drawLine(sqList[i].xPos, sqList[i].yPos + curGrid.length, sqList[i].xPos + curGrid.length , sqList[i].yPos + curGrid.length, paint);
			 canvas.drawLine(sqList[i].xPos + curGrid.length, sqList[i].yPos, sqList[i].xPos + curGrid.length , sqList[i].yPos + curGrid.length, paint);
		 }
		 
		 canvas.drawPath(path, paint); 
		 
		 //Rect rt = new Rect(0,0,(int)curGrid.length, (int)curGrid.length); 
		 for (int i = 0 ; i < litList.size() ; i++) {
			 //if (sqList[i].isLitUp) {
				 //rt = new Rect((int)litList.get(i).xPos,(int)litList.get(i).yPos,(int)curGrid.length, (int)curGrid.length);
				 paint.setColor(Color.BLUE);
				 paint.setStyle(Style.FILL);
				 paint.setAlpha(50);
				 canvas.drawRect((int)litList.get(i).xPos, (int)litList.get(i).yPos, (int)litList.get(i).xPos + (int)curGrid.length, (int)litList.get(i).yPos + (int)curGrid.length, paint); 
			// }
			 
		 }
		 
		 paint.setColor(Color.BLACK);
		 paint.setStyle(Paint.Style.STROKE);
		 paint.setStrokeJoin(Paint.Join.ROUND);
		 
	 } 
		 
	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
		 float eventX = event.getX();
		 float eventY = event.getY();
		 
		// testNum += " " + event.getX() + "  " + event.getY() + " \r\n" ;  
		 
		 switch (event.getAction()) {
	         case MotionEvent.ACTION_DOWN:
	             // Set a new starting point
	             path.moveTo(eventX, eventY);
	             
	             
	             
	             return true;
	         case MotionEvent.ACTION_MOVE:
	             // Connect the points
	             path.lineTo(eventX, eventY);
	             
	             motionSamples(event); 
	             
	             break;
	         default:
	             return false;
		     }
		     // Makes our view repaint and call onDraw
		 	//tempVal = true; 
		     invalidate();
		     return true;

	 }

/** 
	 * function Name: motionSamples(MotionEvent ev)
	   Purpose: registers Historical value of the movement on touch screen
	   Parameters: MotionEvent ev 
	   Return Value: void
	   Local Variables: None
	   		
	Algorithm:	 
	Assistance Received: NONE 
	 * 
	 * 
	   @param 
	   @return 
	*/
	 
	 void motionSamples(MotionEvent ev) { 
			
		     final int historySize = ev.getHistorySize();
		     final int pointerCount = ev.getPointerCount();
		     for (int h = 0; h < historySize; h++) {
		         //System.out.printf("At time %d:", ev.getHistoricalEventTime(h));
		         for (int p = 0; p < pointerCount; p++) {
		             
		        	 float len = curGrid.length; 
		        	 for (int k = 0 ; k < sqList.length ; k++) {
		        		 /*if (sqList[k].isLitUp) {
		        			 continue; 
		        		 }*/
		        		 if (ev.getHistoricalX(p, h) < sqList[k].xPos + len  && ev.getHistoricalX(p, h) > sqList[k].xPos  ) {
		        			 if (ev.getHistoricalY(p, h) < sqList[k].yPos + len  &&  ev.getHistoricalY(p, h) > sqList[k].yPos ) {
		        				 //sqList[k].isLitUp = true; 
		        				 if (!litList.contains(sqList[k])) {
		        					 litList.add(sqList[k]); 
		        				 }
		  
		        			 }
		        		 }
		        	 }
		         }
		     }
		 }
	 
}


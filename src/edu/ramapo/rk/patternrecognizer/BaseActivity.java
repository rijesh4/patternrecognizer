 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;

public class BaseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_base);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.base, menu);
		return true;
	}
	
	public void initTrain(View view) {
		
		Intent intent = new Intent (this,  MainTrainActivity.class) ; 
		startActivity(intent);		
	}
	
	public void initTest (View view) {
		Intent intent = new Intent (this,  MainTestActivity.class) ; 
		startActivity(intent);
	}

}

 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

public class Square {
	
	public float xPos; 
	public float yPos; 
	public float area; 
	public int xRaw; 
	public int yRaw; 
	public boolean isLitUp = false; 
	
	public Square (float x, float y , float area) {
		xPos = x; 
		yPos = y ; 
		this.area = area; 
	}
	
}


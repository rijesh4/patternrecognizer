 /* **************************************************************
     * Name:  Rijesh Kumar                                       *
     * Project:  AI Maze Generator            					 *
     * Class:  AI Spring 2015         			                 *
     * Date:  03/10/2015			                             *
     *************************************************************/

package edu.ramapo.rk.patternrecognizer;

import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainTrainActivity extends Activity {

	//private RelativeLayout container;
	
	private MainDrawingView customCanvas;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_train);

		customCanvas =(MainDrawingView) findViewById(R.id.single_touch_view); 
		
	}

	/*public void initReset (View view) {  
	 
	}*/
	
	public void initReset (View view){
        //container.getChildAt(0).clearCircle();
		customCanvas.initReset(); 
    }
	
	public void initTrain (View view) {
		//customCanvas.setCurVal(); 
		
		Map<Integer, Neuron> aN = NeuralNet.neuralnet.m_neurons;
		
		
		int trainCounter = customCanvas.initTrain(setCurVal ()); 
		
		if (trainCounter != 0 ) {
			int duration = 15000;
			Toast toast = Toast.makeText(this, " "+ trainCounter, duration);
			toast.show();
			//SystemClock.sleep(5000);
			this.finish();
		}
		
	}
	
	public void goBack(View view) {
		customCanvas.curNeuron.saveToFile();
		//Intent intent = new Intent (this, BaseActivity.class) ;
		//startActivity(intent);
		this.finish();
	}
	
	public int setCurVal () {
		 //Inflater.inflate(R.layout.activity_main_train);
		 EditText et = (EditText)findViewById(R.id.e_input); 
		 return   Integer.parseInt(et.getText().toString()); 
	 }
	

}
